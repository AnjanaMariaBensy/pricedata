import requests;
import json;


ticker =["AMD","ADBE","ALGN","AMZN","AMGN","AEP","ADI","ANSS","AAPL","AMAT","ASML","TEAM","ADSK","ATVI","ADP","AVGO","BIDU",
		"BIIB","BMRN","BKNG","CDNS","CDW","CERN","CHKP","CHTR","CPRT","CRWD","CTAS","CSCO","CMCSA","COST","CSX","CTSH","DOCU",
		"DXCM","DLTR","EA","EBAY","EXC","FAST","FB","FISV","FOX","FOXA","GILD","GOOG","GOOGL","HON","ILMN","INCY","INTC","INTU",
		"ISRG","MRVL","IDXX","JD","KDP","KLAC","KHC","LRCX","LULU","MELI","MAR","MTCH","MCHP","MDLZ","MRNA","MNST","MSFT","MU","NFLX",
		"NTES","NVDA","NXPI","OKTA","ORLY","PAYX","PCAR","PDD","PTON","PYPL","PEP","QCOM","REGN","ROST","SIRI","SGEN","SPLK","SWKS",
		"SBUX","SNPS","TCOM","TSLA","TXN","TMUS","VRSN","VRSK","VRTX","WBA","WDAY"]

#ticker =["AMD"]
price_diff = []

for t in ticker:
	apiUrl  = 'https://3p7zu95yg3.execute-api.us-east-1.amazonaws.com/default/priceFeed2?ticker='+ t.strip()+ '&num_days=2'
	response = requests.get(apiUrl)
	data = response.json()
	cur = data['price_data'][1]['value']
	prev = data['price_data'][0]['value']
	diff = cur-prev
	price_diff.append( {"ticker": t.strip(), "percentChange": (diff/cur)*100, "lastClose": cur } )
	   

json_string = json.dumps(price_diff)
with open('priceData.json', 'w') as outfile:
    outfile.write(json_string)
		